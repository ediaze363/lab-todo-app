export class Todo {
  id: number;
  title = '';
  complete = false;

// tslint:disable-next-line: ban-types
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
