import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Todo } from './todo';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SessionService } from './session.service';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient,
    private session: SessionService
  ) { }

  public signIn(username: string, password: string) {
    return this.http.post(API_URL + '/sign-in', {
        username,
        password
      })
      .pipe(catchError(this.handleError));
  }

  // API: GET /todos
  public getAllTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>(API_URL + '/todos', this.getHttpOptions())
      .pipe(catchError(this.handleError));
  }

  // API: POST /todos
  public createTodo(todo: Todo): Observable<Todo> {
    return this.http.post<Todo>(API_URL + '/todos', todo, this.getHttpOptions())
      .pipe(catchError(this.handleError));
  }

  // API: GET /todos/:id
  public getTodoById(todoId: number): Observable<Todo> {
    return this.http.get<Todo>(API_URL + '/todos/' + todoId, this.getHttpOptions())
      .pipe(catchError(this.handleError));
  }

  // API: PUT /todos/:id
  public updateTodo(todo: Todo) {
    return this.http.put<Todo>(API_URL + '/todos/' + todo.id, todo, this.getHttpOptions())
      .pipe(catchError(this.handleError));
  }

  // DELETE /todos/:id
  public deleteTodoById(todoId: number) {
    return this.http.delete<Todo>(API_URL + '/todos/' + todoId, this.getHttpOptions())
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('ApiService::An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`ApiService::Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('ApiService::Something bad happened; please try again later. ' + error);
  }

  private getHttpOptions() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Bearer ' + this.session.accessToken
      })
    };
    return httpOptions;
  }
}
