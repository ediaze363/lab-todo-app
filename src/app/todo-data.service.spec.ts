import { TestBed } from '@angular/core/testing';
import {Todo} from './todo';
import { TodoDataService } from './todo-data.service';
import { ApiService } from './api.service';
import { ApiMockService } from './api-mock.service';

describe('TodoDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      TodoDataService,
      {
        provide: ApiService,
        useClass: ApiMockService
      }
    ]
  }));

  it('should be created', () => {
    const service: TodoDataService = TestBed.get(TodoDataService);
    expect(service).toBeTruthy();
  });

  describe('#getAllTodos()', () => {
    it('should return an empty array by default', () => {
      const service: TodoDataService = TestBed.get(TodoDataService);
      expect(service.getAllTodos()).toEqual([]);
    });

    it('should return all todos', () => {
      const service: TodoDataService = TestBed.get(TodoDataService);
      const todo1 = new Todo({title: 'Hello 1', complete: false});
      const todo2 = new Todo({title: 'Hello 2', complete: true});
      service.addTodo(todo1);
      service.addTodo(todo2);
      expect(service.getAllTodos()).toEqual([todo1, todo2]);
    });
  });

  describe('#save(todo)', () => {
    it('should automatically assign an incrementing id', () => {
      const service: TodoDataService = TestBed.get(TodoDataService);
      const todo1 = new Todo({title: 'Hello 1', complete: false});
      const todo2 = new Todo({title: 'Hello 2', complete: true});
      service.addTodo(todo1);
      service.addTodo(todo2);
      expect(service.getTodoById(1)).toEqual(todo1);
      expect(service.getTodoById(2)).toEqual(todo2);
    });
  });

  describe('#deleteTodoById(id)', () => {
    it('should remove todo with the corresponding id', () => {
      const service: TodoDataService = TestBed.get(TodoDataService);
      const todo1 = new Todo({title: 'Hello 1', complete: false});
      const todo2 = new Todo({title: 'Hello 2', complete: true});
      service.addTodo(todo1);
      service.addTodo(todo2);
      expect(service.getAllTodos()).toEqual([todo1, todo2]);
      service.deleteTodoById(1);
      expect(service.getAllTodos()).toEqual([todo2]);
      service.deleteTodoById(2);
      expect(service.getAllTodos()).toEqual([]);
    });
    it('should not removing anything if todo with corresponding id is not found', () => {
      const service: TodoDataService = TestBed.get(TodoDataService);
      const todo1 = new Todo({title: 'Hello 1', complete: false});
      const todo2 = new Todo({title: 'Hello 2', complete: true});
      service.addTodo(todo1);
      service.addTodo(todo2);
      expect(service.getAllTodos()).toEqual([todo1, todo2]);
      service.deleteTodoById(3);
      expect(service.getAllTodos()).toEqual([todo1, todo2]);
    });
  });

  describe('#updateTodoById(id, values)', () => {
    it('should return todo with the corresponding id and updated data', () => {
      const service: TodoDataService = TestBed.get(TodoDataService);
      const todo = new Todo({title: 'Hello 1', complete: false});
      service.addTodo(todo);
      const changeTodo = new Todo({ id: 1, title: 'new title'});
      const updatedTodo = service.updateTodo(changeTodo);
      updatedTodo.subscribe( data => {
        expect(data.title).toEqual('new title');
      });
    });

    it('should return null if todo is not found', () => {
      const service: TodoDataService = TestBed.get(TodoDataService);
      const todo = new Todo({title: 'Hello 1', complete: false});
      service.addTodo(todo);
      const changeTodo = new Todo({ id: 2, title: 'new title'});
      const updatedTodo = service.updateTodo(changeTodo);
      updatedTodo.subscribe( data => {
        expect(data).toEqual(null);
      });
    });
  });

  describe('#toggleTodoComplete(todo)', () => {
    it('should return the updated todo with inverse complete status', () => {
      const service: TodoDataService = TestBed.get(TodoDataService);
      const todo = new Todo({title: 'Hello 1', complete: false});
      service.addTodo(todo);
      const updatedTodo1 = service.toggleTodoComplete(todo);
      updatedTodo1.subscribe( data1 => {
        expect(data1.complete).toEqual(true);
        const updatedTodo2 = service.toggleTodoComplete(todo);
        updatedTodo2.subscribe( data2 => {
          expect(data2.complete).toEqual(false);
        });
      });
    });
  });
});
